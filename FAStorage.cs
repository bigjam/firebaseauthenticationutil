﻿using System.Collections;
using BicDB.Container;
using BicDB.Variable;
using System;
using UnityEngine;
using Firebase.Auth;
using System.Threading.Tasks;

namespace BicDB.Storage
{
	public class FARecord : RecordContainer{

		#region Enum
		public enum ResultCode
		{
			Success = 0,
			Fail = 1,
			Cancel = 2
		}
		#endregion

		public StringVariable DisplayName = new StringVariable();
		public StringVariable Email = new StringVariable();
		public BoolVariable IsAnonymous = new BoolVariable();
		public BoolVariable IsEmailVerified = new BoolVariable();
		public StringVariable PhotoUrl = new StringVariable();
		public MutableDictionaryContainer ProviderData = new MutableDictionaryContainer();
		public StringVariable ProviderId = new StringVariable();
		public StringVariable RefreshToken = new StringVariable();
		public StringVariable UserId = new StringVariable();         

		private Firebase.Auth.FirebaseAuth auth;

		public FARecord(){
			Debug.Log("init firebase auth");
		
			auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
		}

		public void Join(string _email, string _password){
			auth.CreateUserWithEmailAndPasswordAsync(_email, _password);
		}

		public void Login(string _email, string _password, Action<FARecord, Result> _callback){
			auth.SignInWithEmailAndPasswordAsync(_email, _password).ContinueWith(_task => {
				handleLogin(_task, _callback);
			});
		}

		public void Login(Action<FARecord, Result> _callback){
			Debug.Log("SignInAnonymouslyAsync");
			auth.SignInAnonymouslyAsync().ContinueWith(_task => {
				handleLogin(_task, _callback);
			});		
		}

		private void handleLogin(Task<FirebaseUser> _task, Action<FARecord, Result> _callback){
			if (_task.IsCanceled) {
				Debug.LogError("SignInAnonymouslyAsync was canceled.");
				_callback(this, new Result((int)ResultCode.Cancel));
				return;
			}

			if (_task.IsFaulted) {
				Debug.LogError("SignInAnonymouslyAsync encountered an error: " + _task.Exception);
				_callback(this, new Result((int)ResultCode.Fail));
				return;
			}

			Firebase.Auth.FirebaseUser _user = _task.Result;
			this.DisplayName.AsString = _user.DisplayName;
			this.Email.AsString = _user.Email;
			this.IsAnonymous.AsBool = _user.IsAnonymous;
			this.IsEmailVerified.AsBool = _user.IsEmailVerified;
			this.ProviderId.AsString = _user.ProviderId;
			this.RefreshToken.AsString = _user.RefreshToken;
			this.UserId.AsString = _user.UserId;

			if (_user.PhotoUrl != null) {
				this.PhotoUrl.AsString = _user.PhotoUrl.ToString();
			}

			_callback(this, new Result((int)ResultCode.Success));
		}

		public void Logout(){
			auth.SignOut();
		}

	}

	public class FAStorage : ITableStorage 
	{
		#region Enum

		public enum ResultCode
		{
			Success = 0,
			FailedAsync = 1
		}

		#endregion

		#region Singleton
		static private FAStorage instance = null;
		static public FAStorage GetInstance(){
			if (instance == null) {
				instance = new FAStorage();
			}

			return instance;
		}
		#endregion


		#region Storage

		public void Save<T>(BicDB.ITableContainer<T> _table, Action<Result> _callback, object _parameter) where T : BicDB.IRecordContainer, new ()
		{
			throw new NotImplementedException();
		}

		public void Load<T>(BicDB.ITableContainer<T> _table, Action<Result> _callback, object _parameter) where T : BicDB.IRecordContainer, new ()
		{
			_table.Clear();

			Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;

			auth.SignInAnonymouslyAsync().ContinueWith(task => {
				if (task.IsCanceled) {
					Debug.LogError("SignInAnonymouslyAsync was canceled.");
					return;
				}
				if (task.IsFaulted) {
					Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
					return;
				}


				Firebase.Auth.FirebaseUser newUser = task.Result;
				Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);

			});
		}

		public void Pull<T>(BicDB.ITableContainer<T> _table, Action<Result> _callback, object _parameter) where T : BicDB.IRecordContainer, new ()
		{

		}

		#endregion
	}

//	public class FDDataStoreContainer<T> : BicDB.Container.DataStoreContainer<T> where T : class, BicDB.IRecordContainer, new()
//	{
//		
//	}
}