﻿using UnityEngine;
using System.Collections;
using BicDB.Container;
using System.Threading.Tasks;
using System;
using System.IO;

namespace BicDB.Storage
{
	public class FATableContainer<T> : TableContainer<T> where T : class, IRecordContainer, new()
	{
		public FATableContainer(string _name) : base(_name){

		}

		public void Join(){
		
		}

		public void Login(){

		}

		public void Logout(){
			
		}
	}
}